/* [Bit] */
// The Diameter of the bit shaft. Only for visual purposes.
bit_shaft_diameter = 3.175; // [1:0.001:20]
bit_shaft_length = bit_shaft_diameter * 10;

/* [Fan] */
// Make sure to include some tolerance, e.g. add 0.1mm to the bit_shaft_diameter
fan_hole_diameter = 3.275; // [1:0.001:20]
fan_diameter = 24;         // [10:0.1:50]
fan_height = 5;            // [1:0.1:20]
fan_rim_thickness = 2;     // [0.1:0.1:10]
// Use negative angles for the other direction
fan_blade_angle = 60;    // [-180:1:180]
fan_blade_thickness = 3; // [0.1:0.1:5]
fan_blades = 3;          // [1:1:5]

/* [Precision] */
$fa = 0.5;
$fs = 0.5;

module
shaft()
{
  cylinder(r = bit_shaft_diameter / 2, h = bit_shaft_length, center = true);
}

module ring(diameter = 1, height = 1, thickness = 0.1)
{
  cut_scale = 1.1;
  render() difference()
  {
    cylinder(r = diameter / 2, h = height);
    translate([ 0, 0, -height * (cut_scale - 1) / 2 ])
      cylinder(r = diameter / 2 - thickness, h = height * cut_scale);
  }
}

// Shaft as orientation
% shaft();

// Fan
rotate([ 0, 0, -$t * 360 ]) union()
{
  // inner rim
  ring(diameter = fan_hole_diameter + 2 * fan_rim_thickness,
       height = fan_height,
       thickness = fan_rim_thickness);

  // outer rim
  ring(diameter = fan_diameter,
       height = fan_height,
       thickness = fan_rim_thickness);

  // blades
  for (i = [1:fan_blades]) {
    linear_extrude(height = fan_height, twist = fan_blade_angle, slices = $fn)
      rotate([ 0, 0, (i - 1) / fan_blades * 360 ])
        translate([ fan_hole_diameter / 2, -fan_blade_thickness / 2 ]) square([
          fan_diameter / 2 - fan_rim_thickness - fan_hole_diameter / 2,
          fan_blade_thickness
        ]);
  }
}
